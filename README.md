# FirebaseCloudMessagingHttpV1Api

[Hex.pm](https://hex.pm/packages/firebase_cloud_messaging_http_v1_api) [Documentation](https://hexdocs.pm/firebase_cloud_messaging_http_v1_api/)

Send Firebase Cloud Messaging Push Notifications via the [FCM HTTP v1 API](https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages).

Use [goth](https://github.com/peburrows/goth) to acquire authentication tokens.

## Example

```elixir
{:ok, %Goth.Token{token: token}} =
  Goth.Token.for_scope("https://www.googleapis.com/auth/firebase.messaging")

%Google.Firebase.FCM.V1.Message{
  notification: %Google.Firebase.FCM.V1.Notification{
    title: "notification title",
    body: "notification body"
  },
  android: %Google.Firebase.FCM.V1.AndroidConfig{
    priority: "HIGH",
    ttl: "86400s",
    notification: %Google.Firebase.FCM.V1.AndroidNotification{
      channel_id: "my-android-channel"
    }
  },
  topic: "my-topic"
}
|> Google.Firebase.FCM.V1.Message.send("projects/my-firebase-project", token)
```

## Installation

The package can be installed by adding `firebase_cloud_messaging_http_v1_api` to
your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:firebase_cloud_messaging_http_v1_api, "~> 0.1.0"}
  ]
end
```

## Alternatives

The main alternative is [Pigeon](https://github.com/codedge-llc/pigeon), which
uses the deprecated [Legacy HTTP protocol](https://firebase.google.com/docs/cloud-messaging/http-server-ref). I needed the newer protocol so I could use topics and avoid sending individual messages to each device, so I wrote this library.
