defmodule Google.Firebase.FCM.V1.ApnsConfig do
  @moduledoc """
  Apple Push Notification Service specific options.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#ApnsConfig
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:headers, %{String.t() => String.t()})
    field(:payload, %{String.t() => any()})
    field(:fcm_options, Google.Firebase.FCM.V1.ApnsFcmOptions.t())
  end
end
