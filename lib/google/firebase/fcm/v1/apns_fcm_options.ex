defmodule Google.Firebase.FCM.V1.ApnsFcmOptions do
  @moduledoc """
  Options for features provided by the FCM SDK for iOS.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#apnsfcmoptions
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:analytics_label, String.t())
    field(:image, String.t())
  end
end
