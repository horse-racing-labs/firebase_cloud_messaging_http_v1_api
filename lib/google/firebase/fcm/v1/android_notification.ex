defmodule Google.Firebase.FCM.V1.AndroidNotification do
  @moduledoc """
  Notification to send to android devices.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#androidnotification
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:title, String.t())
    field(:body, String.t())
    field(:icon, String.t())
    field(:color, String.t())
    field(:sound, String.t())
    field(:tag, String.t())
    field(:click_action, String.t())
    field(:body_loc_key, String.t())
    field(:body_loc_args, [String.t()])
    field(:title_loc_key, String.t())
    field(:title_loc_args, [String.t()])
    field(:channel_id, String.t())
    field(:ticker, String.t())
    field(:sticky, boolean())
    field(:event_time, String.t())
    field(:local_only, boolean())

    field(
      :notification_priority,
      :PRIORITY_UNSPECIFIED
      | :PRIORITY_MIN
      | :PRIORITY_LOW
      | :PRIORITY_DEFAULT
      | :PRIORITY_HIGH
      | :PRIORITY_MAX
    )

    field(:default_sound, boolean())
    field(:default_vibrate_timings, boolean())
    field(:default_light_settings, boolean())
    field(:vibrate_timings, [String.t()])
    field(:visibility, :VISIBILITY_UNSPECIFIED | :PRIVATE | :PUBLIC | :SECRET)
    field(:notification_count, integer())
    field(:light_settings, Google.Firebase.FCM.V1.LightSettings.t())
    field(:image, String.t())
  end
end
