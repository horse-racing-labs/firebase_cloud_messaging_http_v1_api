defmodule Google.Firebase.FCM.V1.Notification do
  @moduledoc """
  Basic notification template to use across all platforms.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#Notification
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:title, String.t())
    field(:body, String.t())
    field(:image, String.t())
  end
end
