defmodule Google.Firebase.FCM.V1.Message do
  @moduledoc """
  Message to send by Firebase Cloud Messaging Service.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:name, String.t())
    field(:data, %{String.t() => String.t()})
    field(:notification, Google.Firebase.FCM.V1.Notification.t())
    field(:android, Google.Firebase.FCM.V1.AndroidConfig.t())
    field(:webpush, Google.Firebase.FCM.V1.WebpushConfig.t())
    field(:apns, Google.Firebase.FCM.V1.ApnsConfig.t())
    field(:options, Google.Firebase.FCM.V1.FcmOptions.t())

    # The below fields are a union, only one may be set
    field(:token, String.t())
    field(:topic, String.t())
    field(:condition, String.t())
  end

  @doc """
  Send a message to specified target (a registration token, topic or condition).
  """
  @spec send(Google.Firebase.FCM.V1.Message.t(), String.t(), String.t()) ::
          {:ok, any()} | {:error, String.t()}
  def send(message = %Google.Firebase.FCM.V1.Message{}, parent, access_token)
      when is_binary(parent) and is_binary(access_token) do
    url = "https://fcm.googleapis.com/v1/#{parent}/messages:send"
    body = Jason.encode!(%{message: message})

    headers = [
      {"Content-Type", "application/json"},
      {"Authorization", "Bearer #{access_token}"}
    ]

    HTTPoison.post(url, body, headers)
    |> handle_response()
  end

  defp handle_response({:ok, %HTTPoison.Response{status_code: 200, body: body}}),
    do: Jason.decode(body, keys: :atoms!)

  defp handle_response({:ok, %HTTPoison.Response{body: body}}) do
    %{"error" => %{"message" => message}} = Jason.decode!(body)
    {:error, message}
  end

  defp handle_response({:error, %HTTPoison.Error{reason: reason}}), do: {:error, reason}
end
