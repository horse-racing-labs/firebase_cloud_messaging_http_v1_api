defmodule Google.Firebase.FCM.V1.FcmOptions do
  @moduledoc """
  Platform independent options for features provided by the FCM SDKs.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#fcmoptions
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:analytics_label, String.t())
  end
end
