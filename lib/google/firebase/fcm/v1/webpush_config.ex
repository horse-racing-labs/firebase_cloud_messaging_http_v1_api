defmodule Google.Firebase.FCM.V1.WebpushConfig do
  @moduledoc """
  Webpush protocol options.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#WebpushConfig
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:headers, %{String.t() => String.t()})
    field(:data, %{String.t() => String.t()})
    field(:notification, %{String.t() => any()})
    field(:fcm_options, Google.Firebase.FCM.V1.WebpushFcmOptions.t())
  end
end
