defmodule Google.Firebase.FCM.V1.AndroidFcmOptions do
  @moduledoc """
  Options for features provided by the FCM SDK for Android.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#androidfcmoptions
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:analytics_label, String.t())
  end
end
