defmodule Google.Firebase.FCM.V1.AndroidConfig do
  @moduledoc """
  Android specific options for messages sent through FCM connection server.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#AndroidConfig
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:collapse_key, String.t())
    field(:priority, :NORMAL | :HIGH)
    field(:ttl, String.t())
    field(:restricted_package_name, String.t())
    field(:data, %{String.t() => String.t()})
    field(:notification, Google.Firebase.FCM.V1.AndroidNotification.t())
    field(:fcm_options, Google.Firebase.FCM.V1.AndroidFcmOptions.t())
    field(:direct_boot_ok, boolean())
  end
end
