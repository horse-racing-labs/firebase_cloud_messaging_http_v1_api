defmodule Google.Firebase.FCM.V1.LightSettings do
  @moduledoc """
  Settings to control notification LED.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#LightSettings
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:color, Google.Firebase.FCM.V1.Color.t())
    field(:light_on_duration, String.t())
    field(:light_off_duration, String.t())
  end
end
