defmodule Google.Firebase.FCM.V1.WebpushFcmOptions do
  @moduledoc """
  Options for features provided by the FCM SDK for Web.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#WebpushFcmOptions
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:link, String.t())
    field(:analytics_label, String.t())
  end
end
