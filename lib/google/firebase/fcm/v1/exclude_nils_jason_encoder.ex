defmodule Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder do
  @moduledoc false
  defmacro __using__(_) do
    quote do
      defimpl Jason.Encoder, for: __MODULE__ do
        def encode(value, opts) do
          keys =
            value.__struct__
            |> struct
            |> Map.delete(:__struct__)
            |> Map.keys()

          value
          |> Map.take(keys)
          |> Enum.reject(fn
            {_key, nil} -> true
            _ -> false
          end)
          |> Map.new()
          |> Jason.Encode.map(opts)
        end
      end
    end
  end
end
