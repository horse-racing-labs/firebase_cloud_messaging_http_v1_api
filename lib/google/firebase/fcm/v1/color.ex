defmodule Google.Firebase.FCM.V1.Color do
  @moduledoc """
  Represents a color in the RGBA color space.

  https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#Color
  """

  use Google.Firebase.FCM.V1.ExcludeNilsJasonEncoder
  use TypedStruct

  typedstruct do
    field(:red, float())
    field(:green, float())
    field(:blue, float())
    field(:alpha, float())
  end
end
