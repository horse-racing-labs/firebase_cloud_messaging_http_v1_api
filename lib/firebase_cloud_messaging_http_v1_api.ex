defmodule FirebaseCloudMessagingHttpV1Api do
  @moduledoc """
  Documentation for `FirebaseCloudMessagingHttpV1Api`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> FirebaseCloudMessagingHttpV1Api.hello()
      :world

  """
  def hello do
    :world
  end
end
