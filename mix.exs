defmodule FirebaseCloudMessagingHttpV1Api.MixProject do
  use Mix.Project

  def project do
    [
      app: :firebase_cloud_messaging_http_v1_api,
      version: "0.2.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      docs: docs(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.29", only: :dev, runtime: false},
      {:httpoison, "~> 2.1"},
      {:jason, "~> 1.4"},
      {:typed_struct, "~> 0.3"}
    ]
  end

  defp description do
    """
    Send Firebase Cloud Messaging Push Notifications via the FCM HTTP v1 API.
    """
  end

  defp docs do
    [
      main: "readme",
      extras: ["CHANGELOG.md", "README.md"]
    ]
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Eric Lathrop"],
      licenses: ["MIT"],
      links: %{
        "GitLab" => "https://gitlab.com/horse-racing-labs/firebase_cloud_messaging_http_v1_api"
      },
      source_url: "https://gitlab.com/horse-racing-labs/firebase_cloud_messaging_http_v1_api",
      homepage_url: "https://gitlab.com/horse-racing-labs/firebase_cloud_messaging_http_v1_api"
    ]
  end
end
